Get-ChildItem | Select-Object Name # or gci | Select Name

Get-ChildItem | ForEach-Object {
  Copy-Item -Path $_.FullName -destination .
}
# or gci | % { Copy $_.FullName [directory] }
