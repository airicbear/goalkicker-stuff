# Calling Path.GetFileName()
[System.IO.Path]::GetFileName("C:\Windows\explorer.exe")

# Static methods can be called from the class itself
# Here's an example of a non-static function that will require an instance: 
# [System.DateTime]::AddHours(15) # This will not work because there is no instance of it

# We will need to make an instance of the class first, like so:
$now = [System.DateTime]::Now

# Now, we will be able to use the non-static method from the class
$now.AddHours(15)
