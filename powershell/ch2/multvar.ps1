$fbb = "foo.bar.baz"

# Powershell allows multiple assignment of variables and treats almost everything like an array or list. This means that instead of doing something like this:
$parts = $fbb.Split(".")
$foo = $parts[0]
$bar = $parts[1]
$baz = $parts[2]
$foo
$bar
$baz

# You can simply do this:
$foo, $bar, $baz = $fbb.Split(".")
$foo
$bar
$baz

# Since PowerShell treats assignments in this manner like lists, if there are more values in the list than items in the list of variables to assign them to, the last variable becomes an array of the remaining values. This means you can also do things like this:
$foo, $leftover = $fbb.Split(".") # Sets $foo = "foo", $leftover = ["bar", "baz"]
$foo
$leftover
