# The default scope for a variable is the enclosing container. If outside a script, or other container then the scope is Global.
$foo = "Global Scope"
function myFunc {
  $foo = "Function (local) scope"
  Write-Host $global:foo
  Write-Host $local:foo
  Write-Host $foo
}
myFunc
Write-Host $local:foo
Write-Host $foo
