$intarr = 1,2,4,8,16,32,64,128
$strarr = "Hello",",","world","!"
$intarr
$strarr

# Adding to an array
$intarr += 256

# Combining arrays together
$intstrarr = $intarr + $strarr
$intstrarr
