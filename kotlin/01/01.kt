// Chapter 1: Getting started with Kotlin

// Section 1.1: Hello World

// All Kotlin programs start at the `main` function. Here is an example of a sim
// ple Kotlin "Hello World" program:

// package my.program

fun main(args: Array<String>) {
  println("Hello, world!")
}

// Place the above code into a file named `Main.kt` (this filename is entirely a
// rbitrary)

// When targeting the JVM, the function will be compiled as a static method in a
// class with a name derived from the filename. In the above example, the main c
// lass to run would be `my.program.MainKt`.

// To change the name of the class that contains top-level for a particular file
// , place the following annotation at the top of the file above the package sta
// tement:

@file:JvmName("MyApp")

// In this example, the main class to run would now be `my.program.MyApp`