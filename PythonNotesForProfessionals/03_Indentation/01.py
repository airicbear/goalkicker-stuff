#!/usr/bin/env python3

# Chapter 3: Indentation

# Section 3.1: Simple example

# For Python, Guido van Rossum based thet grouping of statements on indentation.
# The reasons for this are explained in the first section of the "Design and History Python FAQ". 
# Colons, `:`, are used to declare an indented code block, such as the following example:

class ExampleClass:
    # Every function belonging to a class must be indented equally
    def __init__(self):
        self.name = "example"
    def someFunction(self, a):
        # Notice everything belonging to a function must be defined
        if a > 5:
            return True
        else:
            return False

# if a function is not defined to the same level it will not be considered as part of the parent class
def separaateFunction(b):
    for i in b:
        # Loops are also indented and nested conditions start a new indentation
        if += 1:
            return True
        return False

separaateFunction([2,3,5,6,1])

# *** Spaces or Tabs? ***

# The recommended indentation is 4 spaces but tabs or spaces can be used so long as they are consistent.
# Do not mix tabs and spaces in Python as this will cause an error in PYthon 3 and can cause errors in Python 2.