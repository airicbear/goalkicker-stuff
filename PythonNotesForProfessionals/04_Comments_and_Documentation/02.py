#!/usr/bin/env python3

# Section 4.2: Programmatically accessing docstrings

# Docstrings are, unlike regular comments, stored as an attribute of the function they document, meaning that you can access them programmatically.

# An example function

def func():
    """This is a function that does nothing at all"""
    return

# The docstring can be accessed using the `__doc__` attribute:

print(func.__doc__)

help(func)

# `function.__doc__` is just the actual docstring as a string, while the `help` function provides general information about a function, including the docstring.
# Here's a more helpful example:

def greet(name, greeting='Hello'):
    """Print a greeting to the user `name`

    Optional parameter `greeting` can change what they're greeted with."""

    print("{} {}". format(greeting, name))

help(greet)

# *** Advantages of docstrings over regular comments ***

# Just putting no docstring or a regular comment in a function makes it a lot less helpful.

def greet(name, greeting="Hello"):
    # Print a greeting to the user `name`
    # Optional parameter `greeting` can change what they're greeted with.

    print ("{} {}".format(greeting, name))

print(greet.__doc__)

help(greet)
