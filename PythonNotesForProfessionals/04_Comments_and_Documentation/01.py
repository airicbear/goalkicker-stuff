#!/usr/bin/env python3

# Chapter 4: Comments and Documentation

# Section 4.1: Single line, inline and multiline comments

# Comments are used to explain code when the basic code itself isn't clear.

# Python ignores comments, and so will not execute code in there, or raise syntax errors for plain English sentences.

# Single-line comments begin with the hash character (#) and are terminated by the end of the line.

# - Single line comment:

# This is a single line comment in Python

# - Inline comment:

print("Hello World") # This line prints "Hello World"

# - Comments spanning multiple lines have `"""` or `'''` on either end.
# This is the same as a multiline string, but they can be used as comments:

"""
This type of comment spans multiple lines.
These are mostly used for documentation of functions, classes and modules.
"""
