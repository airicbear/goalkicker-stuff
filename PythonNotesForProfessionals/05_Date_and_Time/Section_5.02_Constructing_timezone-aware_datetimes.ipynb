{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Section 5.2: Constructing timezone-aware datetimes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default all `datetime` objects are naive. To make them timezone-aware, you must attach a `tzinfo` object, which provides the UTC offset and timezone abbreviation as a function of date and time.\n",
    "\n",
    "## Fixed Offset Time Zones\n",
    "\n",
    "For time zones that are a fixed offset from UTC, in Python 3.2+, the `datetime` module provides the `timezone` class, a concrete implementation of `tzinfo`, which takes a `timedelta` and an (optional) name paramenter:\n",
    "\n",
    "Python 3.x Version &GreaterEqual; 3.2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2015-01-01 12:00:00+09:00\n",
      "UTC+09:00\n",
      "JST\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime, timedelta, timezone\n",
    "JST = timezone(timedelta(hours=+9))\n",
    "\n",
    "dt = datetime(2015, 1, 1, 12, 0, 0, tzinfo=JST)\n",
    "print(dt)\n",
    "# 2015-01-01 12:00:00+09:00\n",
    "\n",
    "print(dt.tzname())\n",
    "# UTC+09:00\n",
    "\n",
    "dt = datetime(2015, 1, 1, 12, 0, 0, tzinfo=timezone(timedelta(hours=9), 'JST'))\n",
    "print(dt.tzinfo)\n",
    "# JST"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For Python versions before 3.2, it is necessary to use a third party library, such as [`dateutil`](https://dateutil.readthedocs.io/en/stable/). `dateutil` provides an equivalent class, `tzoffset`, which (as of version 2.5.3) takes arguments of the form `dateutil.tz.tzoffset(tzname, offset)`, where `offset` is specified in seconds:\n",
    "\n",
    "Python 3.x Version &lt; 3.2  \n",
    "Python 2.x Version &lt; 2.7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# from datetime import datetime, timedelta\n",
    "# from dateutil import tz\n",
    "\n",
    "# JST = tz.offset('JST', 9 * 3600) # 3600 seconds per hour\n",
    "# dt = datetime(2015, 1, 1, 12, 0, tzinfo=JST)\n",
    "# print(dt)\n",
    "# # 2015-01-01 12:00:00+09:00\n",
    "# print(dt.tzname)\n",
    "# # 'JST'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zones with daylight savings time\n",
    "\n",
    "For zones with daylight savings time, python standard libraries do not provide a standard class, so it is necessary to use a third party library. [`pytz`](http://pytz.sourceforge.net/) and `dateutil` are popular libraries providing time zone classes.\n",
    "\n",
    "In addition to static time zones, `dateutil` provides time zone classes that use daylight savings time (see [the documentation for the tz module](https://dateutil.readthedocs.io/en/stable/tz.html)). You can use the `tz.gettz()` method to get a time zone object, which can then be passed directly to the `datetime` constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2015-01-01 12:00:00-05:00\n",
      "2015-01-01 12:00:00-08:00\n",
      "2015-07-01 12:00:00-07:00\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime\n",
    "from dateutil import tz\n",
    "local = tz.gettz() # Local time\n",
    "PT = tz.gettz('US/Pacific') # Pacific time\n",
    "\n",
    "dt_l = datetime(2015, 1, 1, 12, tzinfo=local) # I am in EST\n",
    "dt_pst = datetime(2015, 1, 1, 12, tzinfo=PT)\n",
    "dt_pdt = datetime(2015, 7, 1, 12, tzinfo=PT) # DST is handled automatically\n",
    "print(dt_l)\n",
    "# 2015-01-01 12:000:00-05:00\n",
    "print(dt_pst)\n",
    "# 2015-01-01 12:00:00-08:00\n",
    "print(dt_pdt)\n",
    "# 2015-07-01 12:00:00-07:00"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**CAUTION:** As of version 2.5.3, `dateutil` does not handle ambiguous datetimes correctly, and will always default to the *later* date. There is no way to construct an object with a `dateutil` timezone representing, for example `2015-11-01 1:30 EDT-4`, since this is *during* a daylight savings time transition.\n",
    "\n",
    "All edge cases are handled properly when using `pytz`, but `pytz` time zones should *not* be directly attached to time zones through the constructor. Instead, a `pytz` time zone should be attached using the time zone's `localize` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2015-01-01 12:00:00-08:00\n",
      "2015-11-01 00:30:00-07:00\n"
     ]
    }
   ],
   "source": [
    "from datetime import datetime, timedelta\n",
    "import pytz\n",
    "\n",
    "PT = pytz.timezone('US/Pacific')\n",
    "dt_pst = PT.localize(datetime(2015, 1, 1, 12))\n",
    "dt_pdt = PT.localize(datetime(2015, 11, 1, 0, 30))\n",
    "print(dt_pst)\n",
    "# 2015-01-01 12:00:00-08:00\n",
    "print(dt_pdt)\n",
    "# 2015-11-01 00:30:00-07:00"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Be aware that if you perform datetime arithmetic on a `pytz`-aware time zone, you must either perform the calculations in UTC (if you want absolute elapsed time), or you must call `normalize()` on the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2015-11-01 03:30:00-07:00\n",
      "2015-11-01 02:30:00-08:00\n"
     ]
    }
   ],
   "source": [
    "dt_new = dt_pdt + timedelta(hours=3) # This should be 2:30 AM PST\n",
    "print(dt_new)\n",
    "# 2015-11-01 03:30:00-07:00\n",
    "dt_corrected = PT.normalize(dt_new)\n",
    "print(dt_corrected)\n",
    "# 2015-11-01 02:30:00-08:00"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
