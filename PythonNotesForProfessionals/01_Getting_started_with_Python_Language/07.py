#!/usr/bin/env python3

# Section 1.7: User Input

# *** Interactive Input ***

# To get input from the user, use the `input` function (note: In Python 2.x, the function is called `raw_input` instead, although Python 2.x has its own version of `input` that is completely different):

# Python 2.x Version >= 2.3

name = raw_input("What is your name?")

# The remainder of this example will be using Python 3 syntax.

# The function takes a string argument, which displays it as a prompt and returns a string.
# The above code provides a prompt, waiting for the user to input.

name = input("What is your name? ")

# If the user types "Bob" and hits enter, the variable name will be assigned to the string "Bob":

name = input("What is your name? ")
print(name)

# Note that the `input` is always of type `str`, which is import if you want the user to enter numbers.
# Therefore, you need to convert the `str` before trying to use it as a number:

x = input("Write a number:")

x / 2

float(x) / 2

# NB: It's recommended to use `try`/`except` blocks to catch exceptions when dealing with user inputs.
# For instance, if your code wants to cast a `raw_input` into an `int`, and what the user writes is uncastable, it raises a `ValueError`.