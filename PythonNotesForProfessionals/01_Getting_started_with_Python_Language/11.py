#!/usr/bin/env python3

# Section 1.11: String function - str() and repr()

# There are two functions that can be used to obtain a readable representation of an object.

# `repr(x)` calls `x.__repr__()`: a representation of `x`.
# `eval` will usually convert the result of this function back to the original object

# `str(x)` calls `x.__str__()`: a human-readable string that describes the object.
# This may elide some technical detail.

# *** repr() ***

# For many types, this function makes an attempt to return a string that would yield an object with the same value when passed to `eval()`.
# Otherwise, the representation is a string enclosed in angle brackets that contains the name of the type of the object along with additional information.
# This often includes the name and address of the object.

# *** str() ***

# For strings, this returns the string itself.
# The difference between this and `repr(object)` is that `str(object)` does not always attempt to return a string that is acceptable to `eval()`.
# Rather, its goal is to return a printable or 'human readable' string.
# If no argument is given, this returns the empty string, ''.

# Example 1:

s = """w'o"w"""
repr(s)
str(s)
eval(str(s))
eval(repr(s))

# Example 2:

import datetime
today = datetime.datetime.now()
str(today)
repr(today)

# When writing a class, you can override these methods to do whatever you want:

class Represent(object):
    def __init__(self, x, y):
        self.x, self.y = x, y
    def __repr__(self):
        return 'Represent(x={},y=\"{}\")'.format(self.x, self.y)
    def __str__(self):
        return "Representing x as {} and y as {}".format(self.x, self.y)

# Using the above class we can see the result:

r = Represent(1, "Hopper")
print(r)
print(r.__repr__)
rep = r.__repr__()
print(rep)
r2 = eval(rep)
print(r2)
print(r2 == r)
