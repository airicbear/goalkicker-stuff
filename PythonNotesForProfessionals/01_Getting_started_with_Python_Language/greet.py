#!/usr/bin/env python3

import hello
hello.say_hello()

# Specific functions of a module can be imported

from hello import say_hello()
say_hello()

# Modules can be aliased.

import hello as ai
ai.say_hello()

# A module can be stand-alone runnable script