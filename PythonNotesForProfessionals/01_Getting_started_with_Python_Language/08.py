#!/usr/bin/env python3

# Section 1.8: Built in Modules and Functions

# A module is a file containing Python definitions and statements.
# Function is a piece of code which execute some logic.

pow(2, 3)

# To check the built in function in Python we can use `dir()`.
# If called without an argument, return the names in the current scope. Else, return an alphabetized list of names comprising (some of) the attribute of the given object, and of attributes reachable from it.

dir(__builtins__)

# To know the functionality of any function, we can use built in function `help`.

help(max)

# Built modules contains extra functionalities.
# For example to get square root of a number we need include `math` module.

import math
math.sqrt(16)

# To know all the functions in a module we can assign the functions list to a variable, and then print the variable.

import math
dir(math)

# It seems __doc__ is useful to provide some documentation in, say, functions

math.__doc__

# In addition to functions, documentation can also be provided in modules.
# So, if you have a file named `helloWorld.py` like this:

class helloWorld:
    """This is the module docstring."""
    def sayHello(self):
        """This is the function docstring."""
        return "Hello World"

# You can access its docstrings like this:
# import helloWorld

helloWorld.__doc__
helloWorld.sayHello.__doc__

# - For any user defined type, its attributes, its class's attributes, and recursively the attributes of its class's base classes can be retrieveed using dir()

class MyClassObject(object):
    pass

dir(MyClassObject)

# Any data type can be simply converted to string using a builtin function called `str`.
# This function is called by default when a data type is passed to `print`

str(123)