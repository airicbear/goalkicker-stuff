#!/usr/bin/env python3

# Section 1.2: Creating variables and assigning values

# To create a variable in Python, all you need to do is specify the variable nam
# e, and then assign a value to it.

# <variable name> = <value>

# Python uses `=` to assign values to variables. There's no need to declare a va
# riable in advance (or to assign a data type to it), assigning a value to a var
# iable itself declares and initializes the variable with that value. There's no
# way to declare a variable without assigning it an initial value

# Integer
a = 2
print("""
a = 2
""" + str(a) + """

type(a)
""" + str(type(a)))

# Integer
b = 9223372036854775807
print("""
b = 9223372036854775807
""" + str(b) + """

type(b)
""" + str(type(b)))

# Floating point
pi = 3.14
print("""
pi = 3.14
""" + str(pi) + """

type(pi)
""" + str(type(pi)))

# String
c = "A"
print("""
c = 'A'
""" + str(c) + """

type(c)
""" + str(type(c)))

# String
name = "John Doe"
print("""
name = 'John Doe'
""" + str(name) + """

type(name)
""" + str(type(name)))

# Boolean
q = True
print("""
q = True
""" + str(q) + """

type(q)
""" + str(type(q)))

# Empty value or null data type
x = None
print("""
x = None
""" + str(x) + """

type(x)
""" + str(type(x)))

# Variable assignment works from left to right. So the following will give you a
# syntax error: 0 = x

# You can not use Python's keywords as a valid variable name. You can see the li
# st of keywords by:

import keyword
print("""
keyword.kwlist
""" + str(keyword.kwlist))

# Rules for variable naming:
# 1. Variable names must start with a letter or an underscore.
# >>  x = True  # valid
# >> _y = True  # valid
# >> 9x = False # invalid (starts with numeral)
# >> $y = False # invalid (starts with symbol)

# 2. The remainder of your variable name may consist of letters, numbers, and un
#    derscores
# >> has_0_in_it = "Still Valid"

# 3. Names are case sensitive
# >> x = 9
# >> y = X + 5
# => NameError: 'X' is not defined

# Even though there's no need to specify a data type when declaring a variable i
# n Python, while allocating the necessary area in memory for the variable, the 
# Python interpreter automatically picks the most suitable built-in type for it.

# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

# Now you know the basics of assignment, let's get this subtlety about assignmen
# t in Python out of the way

# When you use `=` to do an assignment operation, what's on the left of `=` is a
# *name* for the *object* on the right. Finally what `=` does is assign the *ref
# erence* of the object on the right to the name on the left.

# That is:
# >> a_name = an_object # "a_name" is now a name for the reference to the object
# ... "an_object"

# So, from many assignment examples above, if we pick `pi = 3.14`, then `pi` is
# *a* name (not *the* name, since an object can have multiple names) for the ob
# ject `3.14`. If you don't understand something below, come back to this point
# and read this again!

# You can assign multiple values to multiple variables in one line. Note that th
# ere must be the same number of arguments on the right and left sides of the `=
# ` operator:

a, b, c = 1, 2, 3
print("""
a, b, c = 1, 2, 3
""" + str(a), str(b), str(c))

print("""
a, b, c = 1, 2
ValueError: need more than 2 values to unpack""")

print("""
a, b = 1, 2, 3
ValueError: too many values to unpack""")

# The error in the last example can be obviated by assigning remaining values to
# equal number of arbitrary variables. This dummy variable can have any name, bu
# t it is conventional to use the underscore (_) for assigning unwanted values:

a, b, _ = 1, 2, 3
print("""
a, b, _ = 1, 2, 3
""" + str(a), str(b), str(_))

# Note that the number of _ and number of remaining values must be equal. Otherw
# ise 'too many values to unpack error' is thrown as above:

print("""
a, b, _ = 1, 2, 3, 4
ValueError: too many values to unpack (expected 3)""")

# You can also assign a single value to several variables simultaneously.

a = b = c = 1
print("""
a = b = c = 1
""" + str(a), str(b), str(c))

# When using such cascading assignment, it is important to note that all three v
# ariables a, b, and c refer to the *same object* in memory, an `int` object wit
# h the value of 1. In other words, a, b, and c are three different names given 
# to the same int object. Assigning a different object to one of them afterwards
# doesn't change the others, just as expected:

b = 2
print("""
b = 2
""" + str(b))

# The above is also true for mutable types (like `list`, `dict`, etc.) just as i
# t is true for immutable types (like `int`, `string`, `tuple`, etc.)

x = y = [7, 8, 9]
print("""
x = y = [7, 8, 9]
""" + str(x), str(y))

x = [13, 8, 9]
print("""
x = [13, 8, 9]
x: """ + str(x) + """
y: """ + str(y))

# So far so good. Things are a bit different when it comes to *modifying* the ob
# ject (in contrast to *assigning* the name to a differernt object, which we did
# above) when the cascading assignment is used for mutable types. Take a look be
# low, and you will see it first hand:

x = y = [7, 8, 9]
x[0] = 24
print("""
x = y = [7, 8, 9]
x[0] = 24
x: """ + str(x) + """
y: """ + str(y))

# Nested lists are also valid in Python. This means that a list can contain anot
# her list as an element.

x = [1, 2, [3, 4, 5], 6, 7]
print("""
x = [1, 2, [3, 4, 5], 6, 7]
""" + str(x))

# Lastly, variables in Python do not have to stay the same type as which they we
# re first defined--you can simply use `=` to assign a new value to a variable, 
# even if that value is of a different type.

a = 2
a = "New value"
print("""
a = 2
a = "New value"
""" + str(a))

# If this bothers you, think about the fact that what's on the left of `=` is ju
# st a name for an object. First you call the `int` object with a value 2 "a", t
# hen you change your mind and decide to give the name "a" to a `string` object,
# having value 'New value'. Simple, right?