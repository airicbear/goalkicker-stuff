#!/usr/bin/env python3

# Chapter 2: Python Data Types

# Data types are nothing but variables you use to reserve some space in memory.
# Python variables do not need an explicit declaration to reserve memory space.
# The declaration happens automatically when you assign a value to a variable.

# Section 2.1: String Data Type

# String are identified as a contiguous set of characters represented in the quotation marks.
# Python allows for either pairs of signle or double quotes.
# Strings are immutable sequence data type, i.e. each time one makes any changes to a string, completely new string object is created.

a_str = 'Hello World'
print(a_str)      # output will be whole string. Hello World
print(a_str[0])   # output will be first character, H
print(a_str[0:5]) # output will be first five characters. Hello