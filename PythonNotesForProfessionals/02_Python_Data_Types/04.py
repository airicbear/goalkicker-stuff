#!/usr/bin/env python3

# Section 2.4: List Data Type

# A list contains items separated by commas and enclosed within square brackets [].
# Lists are almost similar to arrays in C.
# One difference is that all the items belonging to a list can be of different data types.

list = [123, 'abcd', 10.2, 'd'] # Can be an array of any data type or single data type.
list1 = ['hello','world']
print(list) # will output whole list. [123,'abcd',10.2,'d']
print(list[0:2]) # will output first two elements of list. [123,'abcd']
print(list1 * 2) # will give list1 two times. ['hello','world','hello','world']
print(list + list1) # will give concatentation of both of the lists.
