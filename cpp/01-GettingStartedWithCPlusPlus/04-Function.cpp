#include <iostream>

int add2 (int i) {
    int j = i + 2;
    return j;
}

int add2 (int i, int j) {
    int k = i + j + 2;
    return k;
}

// Default values for function parameters can only be specified in function declarations.
int multiply (int a, int b = 7);
int multiply (int a, int b) {
    return a + b;
}
