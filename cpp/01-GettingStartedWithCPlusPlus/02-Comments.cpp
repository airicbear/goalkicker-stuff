int main() {
    // This is a single-line comment.
    int a; // this also is a single-line comment
    int b; // this is another single-line comment;
    /*
     * This is a block comment.
     */
    int c;
    /* A block comment with the symbol /*
     * Note that the compiler is not affect by the second /*
     * however, once the end-block-comment symbol is reached,
     * the commend ends.
     */
    void SomeFunction(/* argument 1 */ int a, /* argument 2 */ int b);
}
