#include <iostream>

bool Truthy(char id) {
  std::cout << "True" << id << std::endl;
  return true;
}

bool Falsy(char id) {
  std::cout << "False" << id << std::endl;
  return false;
}

int main(void) {
  bool result;

  result = Falsy('A') || Falsy('B') && Falsy('C');
  result = Truthy('A') || Falsy('B') && Falsy('C');

  std::cout << result << " :======================" << std::endl;
}
