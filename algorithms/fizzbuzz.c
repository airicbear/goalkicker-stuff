#include <stdio.h>

void fizz_buzz(int count, int arr[]);
int main(void) {
  int arr[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  int count = 16;
  fizz_buzz(count, arr);
}

void fizz_buzz(int count, int arr[]) {
  for (int num = 0; num < count; num++) {
    if (num % 15 == 0) {
      printf("%d fizz buzz\n", num);
    } else if (num % 3 == 0) {
      printf("%d fizz\n", num);
    } else if (num % 5 == 0) {
      printf("%d buzz\n", num);
    } else {
      printf("%d\n", num);
    }
  }
}