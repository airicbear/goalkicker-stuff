#include <stdio.h>

int find_max (const int *array, size_t len) {
  int max = -2147483648;
  for (size_t i = 0; i < len; i++) {
    if (max < array[i]) {
      printf("Old Max: %d -> New Max: %d\n", max, array[i]);
      max = array[i];
    }
  }
  return max;
}

int main(void) {
  const int array[] = {5,432,54,64,4252,542,77436,5423,654365437,542546,5425};
  size_t len = 11;
  int max_array = find_max(array, len);
  printf("Max: %d", max_array);
}