#include <stdio.h>

_Bool contains_duplicates(const int *array, size_t len) {
  for (int i = 0; i < len - 1; i++) {
    for (int j = i + 1; j < len; j++) {
      if (array[i] == array[j]) {
        return 1;
      }
    }
  }
  return 0;
}

int main(void) {
  const int arr[] = {5, 2, 55, 21, 2};
  size_t len = 5;
  char* hasDuplicates = contains_duplicates(arr, len) ? "Yes" : "No";
  printf("Contains duplicates: %s\n", hasDuplicates);
  return 0;
}