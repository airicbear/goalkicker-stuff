// Chapter 1: Getting started with C# Language

// Section 1.1: Creating a new console application

// 1. Open Visual Studio
// 2. In the toolbar, go to File -> New Project
// 3. Select the Console Application project type
// 4. Open the file `Program.cs` in the Solution Explorer
// 5. Add the following code to Main():

public class Program {
  public static void Main() {
    // Prints a message to the console
    System.Console.WriteLine("Hello, world!");

    /* Wait for the user to press a key. This is a common
       way to prevent the console window from terminating
       and disappearing before the programmer can see the contents
       of the window, when the application is run via Start from within VS. */
    System.Console.ReadKey();
  }
}

// 6. In the toolbar, click Debug -> Start Debugging or hit F5 (running without debugger) to run the programm.

// Explanation
// - `class Program` is a class declaration. The `class Program` contains the da
//   ta and method definitions that your program usese. Classes generally contai
//   n multiple methods. Methods define the behavior of the class. However, the 
//   `Program` class has only one method: `Main`.

// - `static void Main()` defines the `Main` method, which is the entry point fo
//   r all C# programs. The `Main` method states what the class does when execut
//   ed. Only one `Main` method is allowed per class.

// - `System.Console.WriteLine("Hello, world!");` method prints a given data (in
//   this example, `Hello, world!`) as an output in the console window.

// - `System.Console.ReadKey()`, ensures that the program won't close immediatel
//   y after displaying the message. It does this by waiting for the user to pre
//   ss a key on the keyboard. Any key press from the user will terminate the pr
//   ogram. The program terminates when it has finished the last line of code in
//   the `main()` method.

// Using the command line
// To compile via command line use either `MSBuild` or `csc.exe` (the C# compile
// r), both parth of the Microsoft Build Tools package.

// To compile this example, run the following command in the same directory wher
// e `HelloWorld.cs` is located

// \%WINDIR%\\Microsoft.NET\\Framework64\\v4.0.30319\\csc.exe HelloWorld.cs

// It can also be possible that you have two main methods inside one application
// . In this case, you have to tell the compiler which main method to execute by
// typing the following command in the console. (suppose Class `ClassA` also has
// a main method in the same HelloWorld.cs file in HelloWorld namespace)

// \%WINDIR\\Microsoft.NET\\Framework64\\v4.0.30319\\csc.exe HelloWorld.cs /main:HelloWorld.ClassA

// where HelloWorld is namespace

// Note: This is the path where .NET framework 4.0 is located in general. Change
// the path according to your .NET version. In addition, the directory might be 
// framework instead of framework64 if you're using the 32-bit .NET Framework. F
// rom the Windows Command Prompt, you can list all the csc.exe Framework paths 
// by running the following commands (the first for 32-bit Frameworks)

// dir %WINDIR\\Microsoft.NET\\Framework\\csc.exe /s/b
// dir %WINDIR\\Microsoft.NET\\Framework64\\csc.exe /s/b

// There should now be an executable file named `HelloWorld.exe` in the same dir
// ectory. To execute the program from the command prompt, simply type the execu
// table's name and hit `Enter` as follows:

// HelloWorld.exe

// This will produce:

// Hello, world!

// You may also double click the executable and launch a new console window with
// the message "Hello, world!"